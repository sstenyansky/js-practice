// https://learn.javascript.ru/callbacks

// В задаче Анимированный круг находится код для анимации появления круга.

// Давайте представим, что теперь нам нужен не просто круг, а круг с сообщением внутри. И сообщение должно появляться после анимации (когда круг достигнет своих размеров), иначе это будет некрасиво.

// В том решении функция showCircle(cx, cy, radius) рисовала круг, но способа узнать, что всё нарисовано, не было.

// Поэтому добавим в параметры колбэк: showCircle(cx, cy, radius, callback), который выполним, когда анимация будет завершена. Функция callback будет добавлять в наш круг <div> элемент с сообщением.

// Посмотрите пример:

// showCircle(150, 150, 100, div => {
//   div.classList.add('message-ball');
//   div.append("Hello, world!");
// });

function showCircle(cx, cy, radius, cb = () => {}) {
  let div = document.createElement('div');
  
  div.style.textAlign = "center";
  
  div.style.width = 0;
  div.style.height = 0;
  div.style.left = cx + 'px';
  div.style.top = cy + 'px';
  div.className = 'circle';
  document.body.prepend(div);

  setTimeout(() => {
    div.style.width = radius * 2 + 'px';
    div.style.height = radius * 2 + 'px';
    div.style.lineHeight = radius * 2 + 'px';

    div.addEventListener('transitionend', function handler() {
      div.removeEventListener('transitionend', handler);
      cb(div);
    });
  }, 0);
}

showCircle(150, 150, 100, div => {
  div.append("Hello, world!");
});