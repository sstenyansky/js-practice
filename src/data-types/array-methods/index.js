// https://learn.javascript.ru/array-methods

// Напишите функцию camelize(str), которая преобразует строки вида «my-short-string» в «myShortString».

// То есть дефисы удаляются, а все слова после них получают заглавную букву.

// Примеры:

// camelize("background-color") == 'backgroundColor';
// camelize("list-style-image") == 'listStyleImage';
// camelize("-webkit-transition") == 'WebkitTransition';
// P.S. Подсказка: используйте split, чтобы разбить строку на массив символов, потом переделайте всё как нужно и методом join соедините обратно.

function camelize(string) {
  const split = string.split("-");

  return split.map((str, idx) => idx === 0 ? str : str[0].toUpperCase() + str.slice(1)).join('');
}

console.log(camelize("background-color") == 'backgroundColor');
console.log(camelize("list-style-image") == 'listStyleImage');
console.log(camelize("-webkit-transition") == 'WebkitTransition');


// Напишите функцию filterRange(arr, a, b), которая принимает массив arr, ищет в нём элементы между a и b и отдаёт массив этих элементов.

// Функция должна возвращать новый массив и не изменять исходный.

// Например:

// let arr = [5, 3, 8, 1];

// let filtered = filterRange(arr, 1, 4);

// alert( filtered ); // 3,1 (совпадающие значения)

// alert( arr ); // 5,3,8,1 (без изменений)

function filterRange(arr, a, b) {
  return arr.filter(item => item >= a && item <= b);
}

let arr = [5, 3, 8, 1];
let filtered = filterRange(arr, 1, 4);

console.log(filtered); // 3,1 (совпадающие значения)
console.log(arr); // 5,3,8,1 (без изменений)


// Напишите функцию filterRangeInPlace(arr, a, b), которая принимает массив arr и удаляет из него все значения кроме тех, которые находятся между a и b. То есть, проверка имеет вид a ≤ arr[i] ≤ b.

// Функция должна изменять принимаемый массив и ничего не возвращать.

// Например:

// let arr = [5, 3, 8, 1];

// filterRangeInPlace(arr, 1, 4); // удалены числа вне диапазона 1..4

// alert( arr ); // [3, 1]

function filterRangeInPlace(arr, a, b) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] < a || arr[i] > b) {
      arr.splice(i--, 1);
    }
  }
}

let arr1 = [5, 3, 8, 1];

filterRangeInPlace(arr1, 1, 4); // удалены числа вне диапазона 1..4

console.log(arr1); // [3, 1]


// Сортировать по убыванию
// let arr = [5, 2, 1, -10, 8];

// // ... ваш код для сортировки по убыванию

// alert( arr ); // 8, 5, 2, 1, -10

let arr2 = [5, 2, 1, -10, 8];

arr2.sort((a, b) => b - a);

console.log(arr2); // 8, 5, 2, 1, -10


// У нас есть массив строк arr. Нужно получить отсортированную копию, но оставить arr неизменённым.

// Создайте функцию copySorted(arr), которая будет возвращать такую копию.

// let arr = ["HTML", "JavaScript", "CSS"];

// let sorted = copySorted(arr);

// alert( sorted ); // CSS, HTML, JavaScript
// alert( arr ); // HTML, JavaScript, CSS (без изменений)

function copySorted(arr) {
  const copy = arr.slice();

  copy.sort();

  return copy;
}

let arr3 = ["HTML", "JavaScript", "CSS"];

let sorted = copySorted(arr3);

console.log(sorted); // CSS, HTML, JavaScript
console.log(arr3); // HTML, JavaScript, CSS (без изменений)


// Создайте функцию конструктор Calculator, которая создаёт «расширяемые» объекты калькулятора.

// Задание состоит из двух частей.

// Во-первых, реализуйте метод calculate(str), который принимает строку типа "1 + 2" в формате «ЧИСЛО оператор ЧИСЛО» (разделено пробелами) и возвращает результат. Метод должен понимать плюс + и минус -.

// Пример использования:

// let calc = new Calculator;

// alert( calc.calculate("3 + 7") ); // 10
// Затем добавьте метод addMethod(name, func), который добавляет в калькулятор новые операции. Он принимает оператор name и функцию с двумя аргументами func(a,b), которая описывает его.

// Например, давайте добавим умножение *, деление / и возведение в степень **:

// let powerCalc = new Calculator;
// powerCalc.addMethod("*", (a, b) => a * b);
// powerCalc.addMethod("/", (a, b) => a / b);
// powerCalc.addMethod("**", (a, b) => a ** b);

// let result = powerCalc.calculate("2 ** 3");
// alert( result ); // 8
// Для этой задачи не нужны скобки или сложные выражения.
// Числа и оператор разделены ровно одним пробелом.
// Не лишним будет добавить обработку ошибок.

function Calculator() {
  const operators = {
    '+': (a, b) => a + b,
    '-': (a, b) => a - b,
  }

  this.calculate = function (expr) {
    let terms = expr.split(" ").filter(t => t.trim() !== "");

    if (terms.length !== 3) {
      return `Ошибка при вычислении выражения "${expr}": выражение составлено неверно`;
    }
    
    let operator = terms[1];

    if (!operators[operator]) {
      return `Ошибка при вычислении выражения "${expr}": оператор не распознан`;
    }

    let op1 = terms[0];
    let op2 = terms[2];
    
    if (op1 === null || op2 === null || isNaN(+op1) || isNaN(+op2)) {
      return `Ошибка при вычислении выражения "${expr}": один или оба операндов не распознаны`;
    }

    return operators[operator](+op1, +op2);
  }

  this.addMethod = function (name, operation) {
    operators[name] = operation;
  }
}

const calc = new Calculator();

calc.addMethod("*", (a, b) => a * b);
calc.addMethod("/", (a, b) => a / b);
calc.addMethod("**", (a, b) => a ** b);

console.log(calc.calculate("2 + 3"));
console.log(calc.calculate("2 - 3"));
console.log(calc.calculate("2 * 3"));
console.log(calc.calculate("2 / 3"));
console.log(calc.calculate("2 ** 3"));

console.log(calc.calculate("2 x 3"));
console.log(calc.calculate(" + 3"));
console.log(calc.calculate("2 + "));
console.log(calc.calculate("2 3"));
console.log(calc.calculate("2"));
console.log(calc.calculate(""));
console.log(calc.calculate("2 + b"));
console.log(calc.calculate("a + 3"));
console.log(calc.calculate("null + 3"));
console.log(calc.calculate("2 + null"));


// У вас есть массив объектов user, и в каждом из них есть user.name. Напишите код, который преобразует их в массив имён.

// Например:

// let vasya = { name: "Вася", age: 25 };
// let petya = { name: "Петя", age: 30 };
// let masha = { name: "Маша", age: 28 };

// let users = [ vasya, petya, masha ];

// let names = /* ... ваш код */

// alert( names ); // Вася, Петя, Маша

let vasya = { name: "Вася", age: 25 };
let petya = { name: "Петя", age: 30 };
let masha = { name: "Маша", age: 28 };

let users = [ vasya, petya, masha ];

let names = users.map(user => user.name);

console.log(names); // Вася, Петя, Маша


// У вас есть массив объектов user, и у каждого из объектов есть name, surname и id.

// Напишите код, который создаст ещё один массив объектов с параметрами id и fullName, где fullName – состоит из name и surname.

// Например:

// let vasya = { name: "Вася", surname: "Пупкин", id: 1 };
// let petya = { name: "Петя", surname: "Иванов", id: 2 };
// let masha = { name: "Маша", surname: "Петрова", id: 3 };

// let users = [ vasya, petya, masha ];

// let usersMapped = /* ... ваш код ... */

// /*
// usersMapped = [
//   { fullName: "Вася Пупкин", id: 1 },
//   { fullName: "Петя Иванов", id: 2 },
//   { fullName: "Маша Петрова", id: 3 }
// ]
// */

// alert( usersMapped[0].id ) // 1
// alert( usersMapped[0].fullName ) // Вася Пупкин
// Итак, на самом деле вам нужно трансформировать один массив объектов в другой. Попробуйте использовать =>. Это небольшая уловка.

let vasya1 = { name: "Вася", surname: "Пупкин", id: 1 };
let petya1 = { name: "Петя", surname: "Иванов", id: 2 };
let masha1 = { name: "Маша", surname: "Петрова", id: 3 };

let users1 = [ vasya1, petya1, masha1 ];

let usersMapped = users1.map(user => ({id: user.id, fullName: `${user.name} ${user.surname}`}));

console.log(usersMapped);


// Напишите функцию sortByAge(users), которая принимает массив объектов со свойством age и сортирует их по нему.

// Например:

// let vasya = { name: "Вася", age: 25 };
// let petya = { name: "Петя", age: 30 };
// let masha = { name: "Маша", age: 28 };

// let arr = [ vasya, petya, masha ];

// sortByAge(arr);

// // теперь: [vasya, masha, petya]
// alert(arr[0].name); // Вася
// alert(arr[1].name); // Маша
// alert(arr[2].name); // Петя

function sortByAge(users) {
  users.sort((a, b) => a.age - b.age);
}

let vasya2 = { name: "Вася", age: 25 };
let petya2 = { name: "Петя", age: 30 };
let masha2 = { name: "Маша", age: 28 };

let users2 = [ vasya, petya, masha ];

sortByAge(users2);

console.log(users2);


// Напишите функцию shuffle(array), которая перемешивает (переупорядочивает случайным образом) элементы массива.

// Многократные прогоны через shuffle могут привести к разным последовательностям элементов. Например:

// let arr = [1, 2, 3];

// shuffle(arr);
// // arr = [3, 2, 1]

// shuffle(arr);
// // arr = [2, 1, 3]

// shuffle(arr);
// // arr = [3, 1, 2]
// // ...
// Все последовательности элементов должны иметь одинаковую вероятность. Например, [1,2,3] может быть переупорядочено как [1,2,3] или [1,3,2], или [3,1,2] и т.д., с равной вероятностью каждого случая.

function shuffleArray(arr) {
  function randomInt(min, max) {
    return min + Math.floor(Math.random() * (max - min + 1));
  }

  for (i = 0; i < arr.length; i++) {
    const swapIdx = randomInt(i, arr.length - 1);

    let swap = arr[swapIdx];
    arr[swapIdx] = arr[i];
    arr[i] = swap;
  }
}

const arr4 = [1, 2, 3, 4, 5];

function testShuffle(arr, times) {
  const results = {};

  for (let i = 0; i < times; i++) {
    const copy = arr.slice();
    
    shuffleArray(copy);

    const label = copy.join('');

    if (!results[label]) {
      results[label] = 0;
    }

    results[label]++;
  }

  return results;
}

(function () {
  const NUM_TESTS = 1000;

  const results = testShuffle(arr4, NUM_TESTS);

  function fact(n) {
    let accum = 1;

    for (let i = 1; i <= n; i++) {
      accum *= i;
    }

    return accum;
  }

  const permutations = fact(arr4.length);
  const avg = NUM_TESTS / permutations;

  let occurrenceSum = 0;

  for (let result in results) {
    occurrenceSum += results[result];
  }

  const avgOccurrences = occurrenceSum / Object.keys(results).length;

  const deviation = Object.assign({}, results);

  for (let result in deviation) {
    deviation[result] = results[result] / avg;
  }
  
  let deviationSum = 0;

  for (let result in deviation) {
    deviationSum += deviation[result];
  }

  const avgDeviaton = deviationSum /  Object.keys(deviation).length;

  console.log(results);
  console.log(`average deviation from the ideal average number of occurrences (${avg}): ${avgDeviaton} (${avgOccurrences} occurences on average)`);
})();


// Напишите функцию getAverageAge(users), которая принимает массив объектов со свойством age и возвращает средний возраст.

// Формула вычисления среднего арифметического значения: (age1 + age2 + ... + ageN) / N.

// Например:

// let vasya = { name: "Вася", age: 25 };
// let petya = { name: "Петя", age: 30 };
// let masha = { name: "Маша", age: 29 };

// let arr = [ vasya, petya, masha ];

// alert( getAverageAge(arr) ); // (25 + 30 + 29) / 3 = 28

function getAverageAge(arr) {
  return arr.reduce((acc, curr) => acc + curr.age, 0) / arr.length; 
}


let vasya3 = { name: "Вася", age: 25 };
let petya3 = { name: "Петя", age: 30 };
let masha3 = { name: "Маша", age: 29 };

let users3 = [ vasya3, petya3, masha3 ];

console.log( getAverageAge(users3) ); // (25 + 30 + 29) / 3 = 28


// Пусть arr – массив строк.

// Напишите функцию unique(arr), которая возвращает массив, содержащий только уникальные элементы arr.

// Например:

// function unique(arr) {
//   /* ваш код */
// }

// let strings = ["кришна", "кришна", "харе", "харе",
//   "харе", "харе", "кришна", "кришна", ":-O"
// ];

// alert( unique(strings) ); // кришна, харе, :-O

function unique(arr) {
  const result = [];

  for (let item of arr) {
    if (!result.includes(item)) {
      result.push(item);
    }
  }

  return result;
}

let strings = ["кришна", "кришна", "харе", "харе",
  "харе", "харе", "кришна", "кришна", ":-O"
];

console.log( unique(strings) ); // кришна, харе, :-O

