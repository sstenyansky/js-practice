// https://learn.javascript.ru/function-object

// Измените код makeCounter() так, чтобы счётчик мог увеличивать и устанавливать значение:

// counter() должен возвращать следующее значение (как и раньше).
// counter.set(value) должен устанавливать счётчику значение value.
// counter.decrease() должен уменьшать значение счётчика на 1.
// Посмотрите код из песочницы с полным примером использования.

// P.S. Для того, чтобы сохранить текущее значение счётчика, можно воспользоваться как замыканием, так и свойством функции. Или сделать два варианта решения: и так, и так.

function makeCounter() {
  const counterFun = function fn (){
    fn.counter++;
  }

  counterFun.counter = 0;
  counterFun.setCounter = function(value) {
    this.counter = value;
  }

  counterFun.decrease = function() {
    this.counter--;
  }

  return counterFun;
}

const counter = makeCounter();

counter.setCounter(10);
counter();
counter();
counter();

counter.decrease();

console.log(counter.counter);