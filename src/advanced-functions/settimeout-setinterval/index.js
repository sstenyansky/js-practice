// https://learn.javascript.ru/settimeout-setinterval

// Напишите функцию printNumbers(from, to), которая выводит число каждую секунду, начиная от from и заканчивая to.

// Сделайте два варианта решения.

// Используя setInterval.
// Используя рекурсивный setTimeout.

function printNumbers(from, to) {
  let current = from;
  
  const timer = setInterval(function () {
    console.log(current);

    if (++current > to) {
      clearInterval(timer);
    }

  }, 1000);
}

printNumbers(1, 5);

function printNumbersTimeout(from, to) {
  let current = from;
  
  const timer = setTimeout(function fn() {
    console.log(current);

    if (++current > to) {
      clearTimeout(timer);
    } else {
      setTimeout(fn, 1000);
    }
  }, 1000);
}

printNumbersTimeout(1, 5);