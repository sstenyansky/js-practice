// https://learn.javascript.ru/recursion

// Напишите функцию sumTo(n), которая вычисляет сумму чисел 1 + 2 + ... + n.

// Например:

// sumTo(1) = 1
// sumTo(2) = 2 + 1 = 3
// sumTo(3) = 3 + 2 + 1 = 6
// sumTo(4) = 4 + 3 + 2 + 1 = 10
// ...
// sumTo(100) = 100 + 99 + ... + 2 + 1 = 5050
// Сделайте три варианта решения:

// С использованием цикла.
// Через рекурсию, т.к. sumTo(n) = n + sumTo(n-1) for n > 1.
// С использованием формулы арифметической прогрессии.
// Пример работы вашей функции:

// function sumTo(n) { /*... ваш код ... */ }

// alert( sumTo(100) ); // 5050
// P.S. Какой вариант решения самый быстрый? Самый медленный? Почему?

// P.P.S. Можно ли при помощи рекурсии посчитать sumTo(100000)?

function sumToRecursive(n) {
  if (n === 1) {
    return n;
  }

  return n + sumToRecursive(n - 1);
}

function sumToIterative(n) {
  let S = 0;
  
  for (let i = 1; i <= n; i++) {
    S += i;
  }

  return S;
}

function sumToProgression(n) {
  return n * (1 + n) / 2;
}

// console.log(sumToRecursive(10));
// console.log(sumToIterative(10));
// console.log(sumToProgression(10));


// Факториал натурального числа – это число, умноженное на "себя минус один", затем на "себя минус два", и так далее до 1. Факториал n обозначается как n!

// Определение факториала можно записать как:

// n! = n * (n - 1) * (n - 2) * ...*1
// Примеры значений для разных n:

// 1! = 1
// 2! = 2 * 1 = 2
// 3! = 3 * 2 * 1 = 6
// 4! = 4 * 3 * 2 * 1 = 24
// 5! = 5 * 4 * 3 * 2 * 1 = 120
// Задача – написать функцию factorial(n), которая возвращает n!, используя рекурсию.

// alert( factorial(5) ); // 120
// P.S. Подсказка: n! можно записать как n * (n-1)! Например: 3! = 3*2! = 3*2*1! = 6

function factorial(n) {
  if (n === 0 || n === 1) {
    return n;
  }

  return n * factorial(n - 1);
}

// console.log(factorial(5));


// Последовательность чисел Фибоначчи определяется формулой Fn = Fn-1 + Fn-2. То есть, следующее число получается как сумма двух предыдущих.

// Первые два числа равны 1, затем 2(1+1), затем 3(1+2), 5(2+3) и так далее: 1, 1, 2, 3, 5, 8, 13, 21....

// Числа Фибоначчи тесно связаны с золотым сечением и множеством природных явлений вокруг нас.

// Напишите функцию fib(n) которая возвращает n-е число Фибоначчи.

// Пример работы:

// function fib(n) { /* ваш код */ }

// alert(fib(3)); // 2
// alert(fib(7)); // 13
// alert(fib(77)); // 5527939700884757
// P.S. Все запуски функций из примера выше должны работать быстро. Вызов fib(77) должен занимать не более доли секунды.

const memo = [
  1, 1
];

function fib(n) {
  if (n <= 2) {
    return 1;
  }

  if (!memo[n - 1]) {
    memo[n - 1] = fib(n - 1);
  }

  if (!memo[n - 2]) {
    memo[n - 2] = fib(n - 2);
  }

  return memo[n - 1] + memo[n - 2];
}

console.log(fib(3)); // 2
console.log(fib(7)); // 13
console.log(fib(77)); // 5527939700884757


// Допустим, у нас есть односвязный список (как описано в главе Рекурсия и стек):

// let list = {
//   value: 1,
//   next: {
//     value: 2,
//     next: {
//       value: 3,
//       next: {
//         value: 4,
//         next: null
//       }
//     }
//   }
// };
// Напишите функцию printList(list), которая выводит элементы списка по одному.

// Сделайте два варианта решения: используя цикл и через рекурсию.

// Как лучше: с рекурсией или без?

function printListIterative(list) {
  let current = list;

  while (current) {
    console.log(current.value);
    current = current.next;
  }
}

function printListRecursive(list) {
  console.log(list.value);

  if (list.next) {
    printListRecursive(list.next);
  }
}

let list = {
  value: 1,
  next: {
    value: 2,
    next: {
      value: 3,
      next: {
        value: 4,
        next: null
      }
    }
  }
};

printListIterative(list);
printListRecursive(list);


// Выведите односвязный список из предыдущего задания Вывод односвязного списка в обратном порядке.

// Сделайте два решения: с использованием цикла и через рекурсию.

function printListReversedIterative(list) {
  let current = list;

  let elems = [];

  while (current) {
    elems.push(current.value);
    
    current = current.next;
  }

  for (let i = elems.length - 1; i >= 0; i--) {
    console.log(elems[i]);
  }
}

function printListReversedRecursive(list) {
  if (list.next) {
    printListReversedRecursive(list.next);
  }
  
  console.log(list.value);
}

let list1 = {
  value: 1,
  next: {
    value: 2,
    next: {
      value: 3,
      next: {
        value: 4,
        next: null
      }
    }
  }
};

printListReversedIterative(list);
printListReversedRecursive(list);