import Clock from './clock'

class ExtendedClock extends Clock {
  constructor({template = "h:m:s", precision = 1000} = {}) {
    super({template});
    
    this.precision = precision;
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), this.precision);
  }
}

export default ExtendedClock